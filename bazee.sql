-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Окт 07 2021 г., 11:48
-- Версия сервера: 10.6.4-MariaDB
-- Версия PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `bazee`
--

-- --------------------------------------------------------

--
-- Структура таблицы `buildings`
--

CREATE TABLE `buildings` (
  `bid` int(11) NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adress` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contacts` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `buildings`
--

INSERT INTO `buildings` (`bid`, `title`, `author`, `adress`, `contacts`) VALUES
(1, 'БЦ Астаналык', 'Александр Александрович', 'Астана', '87-56-85'),
(2, 'БЦ Москва', 'Лесиков Алексей Михайлович', 'Алматы', '87-85'),
(3, 'БЦ Алматы', 'Новиков Дмитрий Евгеньевич', 'Астана', '87-15'),
(4, 'БЦ Москва', 'Оболенский Василий Борисович', 'Москва', '545-25');

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `FullName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DateFrom` date NOT NULL,
  `DateTo` date NOT NULL,
  `Money` int(11) NOT NULL,
  `ofID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`id`, `FullName`, `DateFrom`, `DateTo`, `Money`, `ofID`) VALUES
(2, 'Орлов Глеб Васильевич', '2021-09-01', '2021-09-17', 54800, 2),
(6, 'gggg', '2020-01-01', '2020-12-12', 55, 2),
(7, 'ffff', '2020-01-02', '2020-09-04', 20, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `offices`
--

CREATE TABLE `offices` (
  `id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `room` int(11) NOT NULL,
  `area` float NOT NULL,
  `status` tinyint(2) NOT NULL,
  `admin` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` int(11) NOT NULL,
  `bcID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `offices`
--

INSERT INTO `offices` (`id`, `level`, `room`, `area`, `status`, `admin`, `options`, `cost`, `bcID`) VALUES
(1, 25, 252, 15.3, 1, 'Новые данные ', 'интернет, Wi-Fi', 1500, 1),
(2, 5, 708, 40, 0, 'Честнов Роберт Валентинович', 'Интернет', 820000, 1),
(3, 1, 1, 1, 1, 'Новые данные ', '1', 1, 1),
(4, 2, 2, 2, 1, 'Адина', '2', 2, 2),
(5, 5, 5, 5, 1, 'hgy', '5', 5, 3),
(6, 75, 27, 2378, 1, 'sdvfgfd', '75', 7575757, 3),
(7, 82, 282, 7572, 1, 'fgjf', '82', 82, 4),
(44, 8, 8, 8, 1, 'Новые данные ', '8', 8, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `request`
--

CREATE TABLE `request` (
  `id` int(10) NOT NULL,
  `ofID` int(11) NOT NULL,
  `bcID` int(11) NOT NULL,
  `clID` int(11) NOT NULL,
  `DateCreate` date NOT NULL,
  `is_active` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Дамп данных таблицы `request`
--

INSERT INTO `request` (`id`, `ofID`, `bcID`, `clID`, `DateCreate`, `is_active`) VALUES
(9, 2, 2, 2, '2021-10-04', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `buildings`
--
ALTER TABLE `buildings`
  ADD PRIMARY KEY (`bid`) USING BTREE;

--
-- Индексы таблицы `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ofID` (`ofID`);

--
-- Индексы таблицы `offices`
--
ALTER TABLE `offices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bcID` (`bcID`);

--
-- Индексы таблицы `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clID` (`clID`),
  ADD KEY `ofID` (`ofID`),
  ADD KEY `bcID` (`bcID`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `buildings`
--
ALTER TABLE `buildings`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `offices`
--
ALTER TABLE `offices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT для таблицы `request`
--
ALTER TABLE `request`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_ibfk_1` FOREIGN KEY (`ofID`) REFERENCES `offices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `offices`
--
ALTER TABLE `offices`
  ADD CONSTRAINT `offices_ibfk_1` FOREIGN KEY (`bcID`) REFERENCES `buildings` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `request`
--
ALTER TABLE `request`
  ADD CONSTRAINT `bcID` FOREIGN KEY (`bcID`) REFERENCES `buildings` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `clID` FOREIGN KEY (`clID`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ofID` FOREIGN KEY (`ofID`) REFERENCES `offices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
