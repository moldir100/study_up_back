"use strict";

const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
require("dotenv").config();
const app = express();
const mysql = require("mysql");
const pool = mysql.createPool(require('./src/utils/constants').dbOptions);
exports.pool = pool;

const port = process.env.PORT || 3306;

const userRoutes = require("./src/routes/Api/users");

const clientRoutes = require("./src/routes/Api/clients");

const officeRoutes = require("./src/routes/Api/offices");

const buildingRoutes = require("./src/routes/Api/buildings");

const requestsRoutes = require("./src/routes/Api/requests");

const coursesRoutes = require("./src/routes/Api/courses");


app.use(bodyParser.urlencoded({ extended: false }));
// app.use(express.urlencoded({ extended: true }));

app.use(bodyParser.json());

app.use(cors({ origin: "*" }));

app.get("/", (req, res) => res.send("study_up"));

// app.use(express.json());

app.use("/user", userRoutes);

app.use("/client", clientRoutes);

app.use("/office", officeRoutes);

app.use("/building", buildingRoutes);

app.use("/requests", requestsRoutes);

app.use("/courses", coursesRoutes);

app.use((err, req, res, next) => {
  console.log(err);
  next();
});

app.listen(port, function (err) {
  if (err) console.log(err);
  console.log("Server listening on PORT", port);
});

