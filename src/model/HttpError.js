class HttpError extends Error {
    constructor(status, error, errorCode) {
        super(error.msg);
        this.code = errorCode || 500;
        this.status = status;
        this.error = error;
    }
}

module.exports = HttpError;