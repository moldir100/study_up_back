'use strict';
var dbConn = require('./../../config/config');

var Building = function (buildings) {
  this.bid         = buildings.bid;
  this.title       = buildings.title;
  this.author      = buildings.author;
  this.adress      = buildings.adress;
  this.city        = buildings.city;
  this.contacts    = buildings.contacts;
};

Building.create = function (newEmp, result) {
  dbConn.query("INSERT INTO buildings set ?", newEmp, function (err, res) {
   if(err) {

     result(err, null);
 }
  else{
   console.log(res);
    result(null, res);
 }
 });
 };
 
Building.findById = function (bid, result) {
 dbConn.query("Select * from buildings WHERE bid = ? ", bid, function (err, res) {
  if(err) {
   console.log("error: ", err);
    result(err, null);
}
else{
  result(null, res);
}
});
};

Building.findAll = function (result) {
  dbConn.query("Select * from buildings ORDER BY bid DESC",
   function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
}
else{
  // console.log('buildings : ', res);
    result(null, res);
}
});
};

Building.update = function(bid, building, result){
 dbConn.query("UPDATE buildings SET title=?,author=?,adress=?, city=?, contacts=? WHERE bid = ?",
  [building.title, building.author, building.adress, building.city, building.contacts, bid], function (err, res) {
  if(err) {
   console.log("error: ", err);
    result(null, err);
}
 else{
  result(null, res);
}
});
};

Building.delete = function(bid, result){
  dbConn.query("DELETE FROM buildings WHERE bid = ?", [bid], function (err, res) {
 if(err) {
  console.log("error: ", err);
    result(null, err);
}
 else{
  result(null, res);
}
});
};

module.exports= Building;