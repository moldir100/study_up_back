'use strict';
const dbConn = require('./../../config/config');

const Courses = function (courses) {
	this.course_id = courses.course_id;
	this.title = courses.title;
	this.description = courses.description;
	this.dateStart = courses.dateStart;
	this.dateEnd = courses.dateEnd;
	this.advantages = courses.advantages;
	this.duration = courses.duration;
	this.type = courses.type;
	this.lessonHours = courses.lessonHours;
	this.courseContacts = courses.courseContacts;
	this.u_id = courses.u_id;
};

Courses.create = function (newCourse, result) {
	dbConn.query("INSERT INTO courses set ?", newCourse, function (err, res) {
		if(err) {
			result(err, null);
		}
		else{
			console.log(res);
			result(null, res);
		}
	});
};

Courses.addCourseLesson = function (newLesson, result) {
	dbConn.query("INSERT INTO lessons set ?", newLesson, function (err, res) {
		if(err) {
			result(err, null);
		}
		else{
			console.log(res);
			result(null, res);
		}
	});
};

Courses.findById = function (u_id, result) {
	dbConn.query("Select * from enrollment Left Join users ON (users.u_id = enrollment.u_id) LEFT JOIN courses ON (enrollment.course_id=courses.course_id) WHERE enrollment.u_id = ? ", u_id, function (err, res) {
		if(err) {
			console.log("error: ", err);
			result(err, null);
		}
		else{
			result(null, res);
		}
	});
};

//после того как выбрал курс -> Get все уроки и задачи этого курса
Courses.courseLessons = function (course_id, result) {
	dbConn.query("Select * from lessons Left Join courses ON (courses.course_id = lessons.course_id) WHERE lessons.course_id = ? ", course_id, function (err, res) {
		if(err) {
			console.log("error: ", err);
			result(err, null);
		}
		else{
			result(null, res);
		}
	});
};

Courses.findTeacherCourses = function (u_id, result) {
	// dbConn.query("Select * from enrollment Left Join users ON (users.u_id = enrollment.u_id) LEFT JOIN courses ON (enrollment.course_id=courses.course_id) WHERE enrollment.u_id = ? ", u_id, function (err, res) {
	dbConn.query("Select * from courses WHERE courses.u_id = ? ", u_id, function (err, res) {
		if(err) {
			console.log("error: ", err);
			result(err, null);
		}
		else{
			result(null, res);
		}
	});
};

//подробнее о данном курсе (напр. Machine Learning)
Courses.aboutCourse = function (course_id, result) {
	// dbConn.query("Select * from enrollment Left Join users ON (users.u_id = enrollment.u_id) LEFT JOIN courses ON (enrollment.course_id=courses.course_id) WHERE enrollment.u_id = ? ", u_id, function (err, res) {
	dbConn.query("Select * from courses JOIN users on (courses.u_id=users.u_id) WHERE courses.course_id = ? ", course_id, function (err, res) {
		if(err) {
			console.log("error: ", err);
			result(err, null);
		}
		else{
			result(null, res);
		}
	});
};

Courses.findAll = function (result) {
	dbConn.query("Select * from courses ORDER BY course_id DESC",
		function (err, res) {
			if(err) {
				console.log("error: ", err);
				result(null, err);
			}
			else{
				// console.log('buildings : ', res);
				result(null, res);
			}
		});
};

Courses.update = function(course_id, course, result){
	dbConn.query("UPDATE courses SET title=?,description=?, u_id=? WHERE course_id = ?",
		[course.title, course.description, course.u_id, course_id], function (err, res) {
			if(err) {
				console.log("error: ", err);
				result(null, err);
			}
			else{
				result(null, res);
			}
		});
};

Courses.delete = function(course_id, result){
	dbConn.query("DELETE FROM courses WHERE course_id = ?", [course_id], function (err, res) {
		if(err) {
			console.log("error: ", err);
			result(null, err);
		}
		else{
			result(null, res);
		}
	});
};

module.exports= Courses;