"use strict";

var dbConn = require("../../config/config");

var Request = function (Requests) {
  this.r_id = Requests.r_id;
  this.name = Requests.name;
  this.phone = Requests.phone;
  this.info = Requests.info;
};

Request.create = function (newReq, result) {
  dbConn.query("INSERT INTO requests set ?", newReq, function (err, res) {
    if (err) {
      result(err, null);
    } else {
      console.log(res);
      result(null, res);
    }
  });
};

Request.findById = function (rid, result) {
  dbConn.query(
    "Select * from request WHERE rid = ? ",
    rid,
    function (err, res) {
      if (err) {
        result(err, null);
      } else {
        result(null, res);
      }
    }
  );
};

Request.findAll = function (result) {
  dbConn.query(
    "Select * from requests",
    function (err, res) {
      if (err) {
        result(null, err);
      } else {
        result(null, res);
      }
    }
  );
};

Request.update = function (rid, request, result) {
  dbConn.query(
    "UPDATE request SET DateCreate=?, is_active=? WHERE rid = ?",
    [request.DateCreate, request.is_active, rid],
    function (err, res) {
      if (err) {
        result(null, err);
      } else {
        result(null, res);
      }
    }
  );
};

Request.delete = function (rid, result) {
  dbConn.query("DELETE FROM request WHERE rid = ?", [rid], function (err, res) {
    if (err) {
      result(null, err);
    } else {
      console.log("successfully");
      result(null, res);
    }
  });
};

module.exports = Request;
