"use strict";
var dbConn = require("./../../config/config");

var Client = function (clients) {
  this.cid = clients.cid;
  this.FullName = clients.FullName;
  this.iin = clients.iin;
  this.birth_date = new Date();
  this.ph_number = clients.ph_number;
};

Client.create = function (newEmp, result) {
  dbConn.query("INSERT INTO clients set ?", newEmp, function (err, res) {
    if (err) {
      console.log("error: ", err);
      result(err, null);
    } else {
      console.log(res);
      result(null, res);
    }
  });
};

Client.findById = function (cid, result) {
  dbConn.query(
    "Select * from clients WHERE cid = ? ",
    cid,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(err, null);
      } else {
        result(null, res);
      }
    }
  );
};

Client.findAll = function (result) {
  dbConn.query("Select * from clients ORDER BY cid DESC", function (err, res) {
    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};

Client.update = function (cid, Clients, result) {
  dbConn.query(
    "UPDATE clients SET FullName=?, birth_date=?, ph_number=?, iin=? WHERE cid = ?",
    [Clients.FullName, Clients.birth_date, Clients.ph_number, Clients.iin, cid],
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        т;
      } else {
        result(null, res);
      }
    }
  );
};

Client.delete = function (cid, result) {
  dbConn.query("DELETE FROM clients WHERE cid = ?", [cid], function (err, res) {
    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};

/*
Client.filter = function (filter, result) {
  dbConn.query("Select * from clients WHERE DateFrom >= ? AND DateTo <= ? AND IsActive = 1", [filter],function (err, res) {
   if(err) {
    console.log("error: ", err);
      result(null, err);
}
else{
  console.log('clients : ', res);
    result(null, res);
}
});
};
*/
module.exports = Client;
