'use strict';
const dbConn = require('./../../config/config');

const User = function (users) {
    this.u_id = users.u_id;
    this.nickName = users.nickName;
    this.email = users.email;
    this.name = users.name;
    this.surname = users.surname;
    this.password = users.password;
    this.contacts = users.contacts;
    this.role_id = users.role_id;
};

User.create = function (newUser, result) {
    dbConn.query("INSERT INTO users set ?", newUser, function (err, res) {
        if(err) {

            result(err, null);
        }
        else{
            console.log(res);
            result(null, res);
        }
    });
};

User.findById = function (u_id, result) {
    dbConn.query("Select * from users WHERE u_id = ? ", u_id, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

User.findAll = function (result) {
    dbConn.query("Select * from users ORDER BY u_id DESC",
        function (err, res) {
            if(err) {
                console.log("error: ", err);
                result(null, err);
            }
            else{
                // console.log('users : ', res);
                result(null, res);
            }
        });
};

// User.login = function (u_id, result) {
//     dbConn.query("Select * from users WHERE u_id = ? ", u_id, function (err, res) {
//         console.log(u_id)
//         if(err) {
//             console.log("error: ", err);
//             result(err, null);
//         }
//         else{
//             result(null, res);
//         }
//     });
// };

User.login = async (value) => {
    let row = await dbConn.query(`SELECT * FROM users WHERE email = ?`, [value]);
    // console.log("row", row)
    if( row) {
        return row[0];
    }
    else {
        console.log("error 1");
    }
};

User.update = function(u_id, users, result){
    dbConn.query("UPDATE users SET name=?,surname=?,password=?, role_id=?",
        [users.name, users.surname, users.password, users.role_id, u_id], function (err, res) {
            if(err) {
                console.log("error: ", err);
                result(null, err);
            }
            else{
                result(null, res);
            }
        });
};

User.delete = function(u_id, result){
    dbConn.query("DELETE FROM users WHERE u_id = ?", [u_id], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            result(null, res);
        }
    });
};

module.exports= User;