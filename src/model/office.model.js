"use strict";

var dbConn = require("./../../config/config");

// var Promise = require('promise');

var Office = function (Offices) {
  this.oid = Offices.oid;
  this.level = Offices.level;
  this.bcID = Offices.bcID;
  this.admin = Offices.admin;
  this.options = Offices.options;
  this.cost = Offices.cost;
  this.area = Offices.area;
  this.room = Offices.room;
};

Office.create = function (newEmp, result) {
  dbConn.query("INSERT INTO offices set ?", newEmp, function (err, res) {
    if (err) {
      console.log("error: ", err);
      result(err, null);
    } else {
      console.log(res);
      result(null, res);
    }
  });
};

Office.findById = function (oid, result) {
  dbConn.query(
    "Select * from offices where oid = ? ",
    [oid],
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(err, null);
      } else {
        result(null, res);
      }
    }
  );
};

Office.findAll = function (result) {
  dbConn.query(
    "Select o.area, o.level, o.options, o.cost, o.oid, o.room, o.admin, o.bcID, b.title, r.is_active from offices o left join request r on (o.oid=r.ofID) join buildings b on (o.bcID=b.bid) GROUP BY o.oid ORDER BY oid DESC",
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        result(null, res);
      }
    }
  );
};

Office.update = function (oid, offices, result) {
  dbConn.query(
    "UPDATE offices SET level=?, cost=?, room=?, area=?, options=?,admin=? WHERE oid= ?",
    [
      offices.level,
      offices.cost,
      offices.room,
      offices.area,
      offices.options,
      offices.admin,
      oid,
    ],
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        result(null, res);
      }
    }
  );
};

Office.delete = function (oid, result) {
  dbConn.query("DELETE FROM offices WHERE oid = ?", [oid], function (err, res) {
    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      console.log("successfully");
      result(null, res);
    }
  });
};

// Office.filter = function (filter, result) {
//   dbConn.query("Select * from Offices WHERE DateFrom >= ? AND DateTo <= ? AND IsActive = 1", [filter],function (err, res) {
//    if(err) {
//     console.log("error: ", err);
//       result(null, err);
// }
// else{
//   console.log('Offices : ', res);
//     result(null, res);
// }
// });
// };

module.exports = Office;
