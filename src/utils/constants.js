require('dotenv').config();
const HttpError = require('../model/HttpError');
const fs = require("fs");
const log = console.log;

const mode = '' + process.env.NODE_ENV === 'prod';

const ORIGINS = [
    'http://localhost:8080',
    // 'https://kjtdist.loc',
    // 'http://localhost:8082',
    // 'https://cb.kjt.kz',
    // 'https://dev.kjt.kz',
    // 'https://docs.kjt.kz',
    // 'https://kjt.kz'
];

const CTYPEIP = /(http|https):\/\/192.168.[0-9]{1,3}.[0-9]{1,3}:(8085|8086)$/;
const BTYPEIP = /(http|https):\/\/172.20.[0-9]{1,3}.[0-9]{1,3}:8085$/;

exports.originOptions = (o, cb) => {
    if (!o) { // we can test in dev mode, but cant in prod
        log('origin bye-bye:', o ? o : null); // origin is null, (postman)
        return mode ? cb(corsError, false) : cb(null, true);
    } else {
        if (ORIGINS.includes(o) || CTYPEIP.test(o) === true || BTYPEIP.test(o) === true) {
            return cb(null, true);
        }
        log('origin bye-bye:', o);
        cb('bye-bye', false);
    }
};

exports.dbOptions = {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_DB,
    connectionLimit: process.env.DB_CONN_LIM,
};

// exports.serverOptions = {
//     key: fs.readFileSync(`${process.env.SSL_KEY}`, 'utf8'),
//     cert: fs.readFileSync(`${process.env.SSL_CERT}`, 'utf8')
// }

exports.ioOptions = {
    serveClient: false,
    pingInterval: 10000,
    pingTimeout: 5000,
    upgradeTimeout: 20000,
    allowEIO3: true,
}

exports.textOptions = {format: 'A4', margin: {top: '1.5cm', right: '1cm', bottom: '1.5cm', left: '1cm'}}

exports.signOptions = (hash) => {
    return {
        displayHeaderFooter: true,
        footerTemplate: `
            <div style="color: lightgray; border-top: solid lightgray 1px; font-size: 10px; padding-top: 5px; text-align: center; width: 100%;">
                <span>Document hash: ${hash}</span>
            </div>`,
        headerTemplate: `<div></div>`
    }
}

exports.updateErr = new HttpError(false, {code: 'server_error', msg: 'Ошибка при обновлении'}, 200);
exports.deleteErr = new HttpError(false, {code: 'server_error', msg: 'Ошибка при удалении'}, 200);
exports.addErr = new HttpError(false, {code: 'server_error', msg: 'Ошибка при создании'}, 200);
exports.authErr = new HttpError(false, {code: 'unauthorized', msg: 'Не авторизован'}, 200);
exports.tokenExpiredErr = new HttpError(false, {code: 'unauthorized', msg: 'Сессия неактивна'}, 200);
exports.tokenErr = new HttpError(false, {code: 'unauthorized', msg: 'Ошибка в данных сессии'}, 200);
exports.offerErr = new HttpError(false, {code: 'not_offer', msg: 'Вы не приняли условия публичной оферты'}, 200);
exports.fieldErr = new HttpError(false, {code: 'no_fields', msg: 'Недостаточно полей'}, 200);
exports.notFoundErr = new HttpError(false, {code: 'not_found', msg: 'Информация не найдена'}, 200);
exports.serverErr = new HttpError(false, {code: 'server_error', msg: 'Ошибка на сервере'}, 200);
exports.wrongPassErr = new HttpError(false, {code: 'wrong_password', msg: 'Неверный пароль'}, 200);
exports.notValidErr = new HttpError(false, {code: 'not_valid', msg: 'Выбранные вами ключи не являются ключами подписи'}, 200);
exports.keyExpiredErr = new HttpError(false, {code: 'not_valid', msg: 'Срок жизни ваших ключей истек'}, 200);
exports.contractNotFound = new HttpError(false, {code: 'not_found', msg: 'Информация по договору не найдена'}, 200);
exports.notVerifiedErr = new HttpError(false, {code: 'not_found', msg: 'Выбранные вами ключи не имеют прав для подписи договоров'}, 200);
exports.managerErr = new HttpError(false, {code: 'not_enough_credentials', msg: 'Недостаточно прав'}, 200);
exports.tariffErr = new HttpError(false, {code: 'not_enough_credentials', msg: 'Вы не можете выбирать этот тариф для этой станции'}, 200);
exports.credErr = new HttpError(false, {code: 'wrong_credentials', msg: 'Выбранные вами ключи принадлежат другой компании'}, 200);
exports.ocspErr = new HttpError(false, {code: 'wrong_credentials', msg: 'Выбранные вами ключи отозваны'}, 200);
exports.actionErr = new HttpError(false, {code: 'wrong_credentials', msg: 'Вы не можете совершить это действие'}, 200);
exports.signedErr = new HttpError(false, {code: 'wrong_credentials', msg: 'Нельзя редактировать, так как подписан одной из сторон!'}, 200);
exports.dateErr = new HttpError(false, {code: 'not_enough_credentials', msg: 'Вы не можете выбрать эту дату'}, 200);
exports.requisiteErr = new HttpError(false, {code: 'not_enough_credentials', msg: 'Вы не можете отредактировать этот реквизит, потому что используется в договорах'}, 200);
exports.contactErr = new HttpError(false, {code: 'not_enough_credentials', msg: 'Вы не можете отредактировать этот контакт, потому что используется в договорах'}, 200);
exports.agreementErr = new HttpError(false, {code: 'wrong_credentials', msg: 'Вы не можете создать доп.соглашение на этот договор, так как договор еще не подписан'}, 200);
const corsError = new HttpError(false, {code: 'bye-bye', msg: 'bye-bye'}, 500);

exports.docParagraphStyle = [
    {id: "tab", name: "tab", run: {size: 24},},
    {id: "text", name: "text", run: {size: 26, bold: true}, paragraph: {spacing: {before: 240, after: 360},}}
]

exports.monthNames = ["январь", "февраль", "март", "апрель", "май", "июнь",
    "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"
];

exports.reasonToSign = (id) => {
    if (id === 1) return 'Доверенности';
    if (id === 2) return 'Устава';
    if (id === 3) return 'Свидетельства';
}

exports.manTariffs = (id) => {
    if (id ===1)return 7836
    if (id === 2) return 9916;
    if (id === 3 || id === 4 || id === 5) return 9403;

}

exports.CAN_SIGN = ['CEO', 'CAN_SIGN', 'INDIVIDUAL'];

exports.CLIENT_TO_SIGN = 1;
exports.MANAGER_TO_SIGN = 3;
exports.MANAGER_EDITED = 2;
exports.SIGNED = 4;
exports.PAPER_CONTRACT = 5;
exports.EXPIRED = 6;

exports.WAGON_STATUS = {
    WORKING: 2,
    DONE: 3
}

exports.WAGON_STATE = {
    DELETED: 0,
    ACTIVE: 1
}

exports.REQUEST_STATUS = {
    WORKING: 5,
    DONE: 6
}

exports.NATURLIST_EXISTS = {
    MSG: 'Невозможно удалить, т.к. оформлена натурка на уборку на этот вагон',
    CODE: 'cant_delete',
    STATUS: 200
}
