const Util = require("util");
const fs = require("fs");
const crypto = require("crypto");
exports.findOcc = (arr, key) => {
  let arr2 = [];
  arr.forEach(x => {
    if (arr2.some(val => val[key] == x[key])) {
      arr2.forEach(k => {
        if (k[key] === x[key]) k['occurrence']++;
      });
    } else {
      let a = {};
      a[key] = x[key];
      a['occurrence'] = 1;
      arr2.push(a);
    }
  });
  return arr2;
}

exports.omit = (obj, ...props) => {
  const result = { ...obj };
  props.forEach(function(prop) {
    delete result[prop];
  });
  return result;
}

exports.flat = function flat(source, target) {
  Object.keys(source).forEach(function (k) {
    if (source[k]!== null && typeof source[k] === 'object') {
      flat(source[k], target);
      return;
    }
    target[k] = source[k];
  });
}

exports.snakeToCamel = function(data, depth) {
  if (!Util.isObject(data)) return _camelize(data)
  if (typeof depth === 'undefined') depth = 1;
  return _processKeys(data, _camelize, depth);
};

/**
 * @param {Object|String} data string or keys of object are named in form of camel case
 * @param {number} depth to which level of keys should it process
 * @return {Object|String} string or keys of object are named in form of snake
 */
exports.camelToSnake = function(data, depth) {
  if (!Util.isObject(data)) return _snakelize(data);
  if (typeof depth === 'undefined') depth = 1;
  return _processKeys(data, _snakelize, depth);
};

// snakelize a string formed in underscore
function _snakelize(key) {
  return key.split(/(?=[A-Z])/).join('_').toLowerCase();
}

// camelize a string formed in underscore
function _camelize(key) {
  if (Util.isNumber(key)) return key;
  key = key.replace(/[\-_\s]+(.)?/g, (match, ch) => ch ? ch.toUpperCase() : '');
  // Ensure 1st char is always lowercase
  return key.substr(0, 1).toLowerCase() + key.substr(1);
}

// camelize/snakelize keys of an object
// @param {number} depth to which level of keys should it process
function _processKeys(obj, processer, depth) {
  if (depth === 0 || !Util.isObject(obj)) return obj;
  let result = {};
  let keys = Object.keys(obj);
  for (let i = 0; i < keys.length; i++) {
    result[processer(keys[i])] = _processKeys(obj[keys[i]], processer, depth - 1);
  }
  return result;
}

exports.getShaSum = (file, isFile) => {
  const fileBuff =isFile ? fs.readFileSync(file) : file;
  const hashSum = crypto.createHash('sha256');
  hashSum.update(fileBuff)
  return hashSum.digest('hex');
}