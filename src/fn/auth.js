// const jwt = require('jsonwebtoken');
// const md5 = require('md5');
// const fs = require('fs');
// const Parallel = require('async-parallel');
// const {QueryTypes} = require("sequelize");
// const jwt = require('jsonwebtoken');

const helpers = require('../utils/helper');
const consts = require('../utils/constants')
// const dbcon = require("./db");
// const dbcon = require("mysql");
// const dbcon = require("server");

// const {sequelize} = require('./src/model')
const moment = require("moment");
const dbcon = require("../../server");
// const Parallel = require('async-parallel');


async function auth(email, password) {
  let result = {
    status: false,
    token: null,
    user: null,
    response: {},
    rooms: [],
  }
  try {
    // const user = await dbcon.pool.query("SELECT  * FROM `users` WHERE us.`email`=? AND us.`password`=? ", [email, md5(password + process.env.SAULT)]);
    const user = await dbcon.pool.query("SELECT  * FROM `users` WHERE us.`email`=? AND us.`password`=? ", [email, password + process.env.SAULT]);
    if (user.length === 1) {
      let singInfo = {};
      result.lists = {
        user: [],
      };
      result.lists.user.push(user)

      result.status = true;
      const userData = user[0];
      userData['offer'] = userData['offerDt'] ? 1 : 0;
      delete userData['offerDt'];
      result.user = userData;
      result.sub = "auth";
      result.exp = Math.floor(Date.now()) + (86400 * 1000);
      result.iat = Date.now();
      const privateKey = fs.readFileSync(`${process.env.SSL_KEY}`, 'utf8');

      singInfo.u_id = user[0].u_id;
      singInfo.nickName = user[0].nickName;
      singInfo.email = user[0].email;
      singInfo.name = user[0].name;
      singInfo.surname = user[0].surname;
      singInfo.role_id = user[0].role_id;
      singInfo.sub = "auth";

      let token = jwt.sign(singInfo, privateKey, {algorithm: 'RS512', expiresIn: '1 day'});

      result.rooms.push(`u_id${user[0].u_id}`);
      result.rooms.push(`nickName${user[0].nickName}`);
      result.rooms.push(`email${user[0].email}`);
      result.rooms.push(`name${user[0].name}`);
      result.rooms.push(`surname${user[0].surname}`);
      result.rooms.push(`role_id${user[0].role_id}`);

      result.token = token;
      result.response.token = token;
    }
  } catch (e) {
    console.log('auth error:', e);
  } finally {
    return result;
  }
}

async function checkToken(token) {
    try {
      if (!token) throw consts.authErr
      return jwt.verify(token, fs.readFileSync(`${process.env.SSL_CERT}`, 'utf8'), {algorithms: ['RS512']});
    } catch (e) {
      if (e instanceof jwt.TokenExpiredError) throw consts.tokenExpiredErr
      if (e instanceof jwt.JsonWebTokenError) throw consts.tokenErr
      throw consts.authErr
    }
}

exports.acceptOffer = async (id) => {
  try {
    const offerDt = await dbcon.pool.query(`SELECT offer_dt AS offerDt FROM users WHERE id=${id}`);
    if (!offerDt[0].offerDt) {
      await dbcon.pool.query(`UPDATE users SET offer_dt=NOW() WHERE id=${id};`);
      return await dbcon.pool.query(`SELECT offer_dt AS offerDt FROM users WHERE id=${id}`);
    }
    return offerDt;
  } catch(e) {
    throw e;
  }
}

exports.getLastChange = async (stationId) => {
  return await sequelize.query("SELECT c.`dt_close` as dtClose, c.`day`, st.morning, st.night, st.interim FROM changes c JOIN stations st ON st.id=c.station_id WHERE c.station_id=? ORDER BY c.id DESC LIMIT 1;",{replacements:[stationId], type: QueryTypes.SELECT, plain: true});
}

exports.signup = async ({login, password, roleId, companyId, stationId, name}) => {
  const doesExist = await dbcon.pool.query('select * from users where login=?;', [login]);
  if (doesExist[0].login === login) return false;
  const hash = md5(password+process.env.SAULT);
  return await dbcon.pool.query("INSERT INTO users (name, role_id, company_id, station_id, login, password) VALUES (?,?,?,?,?,?)", [name, roleId, companyId, stationId, login, hash]);

}

module.exports.auth = auth;
module.exports.checkToken = checkToken;
