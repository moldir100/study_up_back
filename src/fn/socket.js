let sio = require('socket.io');
let io = null;

exports.io = function () {
	return io;
};

exports.initialize = function(server, options = {}) {
	io = sio(server, options);
};
