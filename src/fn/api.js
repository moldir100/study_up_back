const axios = require('axios').default;

const SERVER_URL = process.env.NCANODE_URL;
const TELEGRAM_URL = 'https://api.telegram.org/bot2118129990:AAEoMm2RcuGE-xAfUnFCty9WZ26zDuhkyI4/sendMessage';

exports.xmlSign = async (data, p12, password) => {
  try {
    const result = await axios.post(SERVER_URL,
        {version: '1.0', method: 'XML.sign', params: {xml: data, p12, password}});
    return result.data;
  } catch(e) {
    throw e;
  }
}

exports.tspSign = async (raw) => {
  try {
    const result = await axios.post(SERVER_URL, {version: '1.0', method: 'TSP.sign', params: {raw}});
    return result.data;
  } catch(e) {
    throw e;
  }
}

exports.tspVerify = async (raw, cms) => {
  try {
    const result = await axios.post(SERVER_URL, {version: '1.0', method: 'TSP.verify2', params: {raw, cms}});
    return result.data;
  } catch(e) {
    throw e;
  }
}

exports.xmlVerify = async (xml) => {
  try {
    const result = await axios.post(SERVER_URL, {version: '1.0', method: 'XML.verify', params: {xml, verifyOcsp: true}})
    return result.data;
  } catch(e) {
    throw e;
  }
}

exports.keyInfo = async (p12, password) => {
  try {
    const result = await axios.post(SERVER_URL, {version: '1.0', method: 'PKCS12.info', params: {p12, password}})
    return result.data;
  } catch(e) {
    throw e;
  }
}

exports.logToTelegram = async (t, m) => await axios.post(TELEGRAM_URL, {chat_id: -1001541025282, text:`${m ? '[PROD]': '[DEV]'}: `+t, parse_mode: 'HTML'})
