const express = require("express");

const router = express.Router();
const Requests = require("../../controller/requests.controller");

router.get("/", Requests.findAll);

router.post("/", Requests.create);

router.get("/:rid", Requests.findById);

router.put("/:rid", Requests.update);

router.delete("/:rid", Requests.delete);

module.exports = router;
