const express = require('express')

const router = express.Router()

const courses = require('../../controller/courses.controller');

const auth = require("C:\\Users\\Moldir\\Desktop\\DIPLOMA WORK\\study_up_back\\src\\middlewares\\auth.js");

router.get('/', courses.findAll);


// курсы для каждого студента
router.get('/:u_id', courses.findById);

//курсы от teachers
router.get('/teacher/:u_id', courses.findTeacherCourses);

//подробнее -- это лендинг
router.get('/aboutCourse/:course_id', courses.aboutCourse);

// курс -> материалы и задачи этого круса
router.get('/subject/:course_id', courses.courseLessons)

//добавить урок
router.get('/subject/addLesson/:course_id', courses.addCourseLesson)


//delete course
router.delete('/delete/:course_id', courses.delete);

//скачать пдф
// router.get('/subject/:course_id', courses.courseLessons)

router.post('/add', courses.create);

router.put('/:bid', courses.update);

module.exports = router

