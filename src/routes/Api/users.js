const express = require('express')

const dotenv = require('dotenv');

const router = express.Router()

const Users = require('../../controller/user.controller');
const validators = require('../../middlewares/validate');
const dbConn = require("../../../config/config");
const fs = require("fs");
const {config} = require("dotenv");
const jwt = require('jsonwebtoken');

dotenv.config();
// const bcrypt = require('bcryptjs');
// const authController = require('../controllers/auth');
// const loginFailed = {status: false, token: null, user: null};

// router.get('/', user.findAll);

const auth = require("C:\\Users\\Moldir\\Desktop\\DIPLOMA WORK\\study_up_back\\src\\middlewares\\auth.js");
const TOKEN_SECRET = '1a2b-3c4d-5e6f-7g8h'
router.post('/login', (req, res, next) => {
	// Get user input
	const {email, password} = req.body;

	// Validate user input
	if (!(email && password)) {
		res.status(400).send("All input is required");
	}

	dbConn.query(
		// `SELECT * FROM users WHERE email = ${dbConn.escape(req.body.email)};`,
		// 'SELECT * FROM users WHERE email = ? AND password = ?', [req.body.email, req.body.password],
		'SELECT * FROM users WHERE email = ?', [req.body.email],

		// SELECT * FROM `users` WHERE us.`login`=? AND us.`password`=? AND us.`is_active`=1;", [login, md5(password + process.env.SAULT)]);


		(err, result) => {
			console.log("result", result)
// user does not exists
			if (err) {
				throw err;
				return res.status(400).send({
					msg: err
				});
			}
			if (!result.length) {
				return res.status(401).send({
					msg: 'Email or password is incorrect! 1'
				});
			}

			function generateAccessToken(user) {
				return jwt.sign(user, TOKEN_SECRET, { expiresIn: '1800s' });
			}

			const token = generateAccessToken({ user: result[0] });
			// res.json(token);

			let data = {
				u_id: '',
				name: '',
				surname: '',
				nickName: '',
				email: '',
				role_id: '',
			}
			data.u_id = result[0].u_id
			data.name = result[0].name
			data.surname = result[0].surname
			data.nickName = result[0].nickName
			data.email = result[0].email
			data.role_id = result[0].role_id

			if(result){
				if(req.body.password === result[0].password){
					return res.status(200).send({
						token: token,
						status: true,
						msg: 'Logged in!',
						user: data,
					});
				}else{
					return res.status(401).send({
						msg: 'Username or password is incorrect!',
						status: false
					});
				}
			}
		}
	);
});

router.post('/logout', (req, res, next) => {
	return res.status(200).send({
		status: true,
		msg: 'Logged out!',
	});
});

router.post("/welcome", auth, (req, res) => {
	// res.status(200).send("Welcome 🙌 ", res);
	console.log("res.user", res.user)
	let data = {
		u_id: '',
		name: '',
		surname: '',
		nickName: '',
		email: '',
		role_id: '',
		address: '',
		about: '',
		contacts: '',
	}
	data.u_id = res.user.u_id
	data.name = res.user.name
	data.surname = res.user.surname
	data.nickName = res.user.nickName
	data.email = res.user.email
	data.role_id = res.user.role_id
	data.address = res.user.address
	data.about = res.user.about
	data.contacts = res.user.contacts

	res.status(200).send({
		status: true,
		msg: 'Authenticated',
		user: data
	});
});


// const authenticateJWT = (req, res, next) => {
// 	const authHeader = req.headers.authorization;
//
// 	if (authHeader) {
// 		const token = authHeader.split(' ')[1];
//
// 		jwt.verify(token, TOKEN_SECRET, (err, user) => {
// 			if (err) {
// 				return res.sendStatus(403);
// 			}
//
// 			req.user = user;
// 			next();
// 		});
// 	} else {
// 		res.sendStatus(401);
// 	}
// };

router.post('/check', (req, res, next) => {
	const authHeader = req.body.token;

	if (authHeader) {
		const token = authHeader.split(' ')[1];

		jwt.verify(token, TOKEN_SECRET, (err, user) => {
			if (err) {
				return res.sendStatus(403);
			}
			req.user = user;
			res.sendStatus(200);
			next();
		});
	} else {
		res.sendStatus(401);
	}

});


router.post('/register', Users.create);

router.put('/:u_id', Users.update);

router.delete('/:u_id', Users.delete);

module.exports = router
