const express = require('express')

const router = express.Router()

const client = require('../../controller/client.controller');

    // router.get('/filter', client.filter);

    router.get('/', client.findAll);

    router.post('/', client.create);

    router.get('/:сid', client.findById);

    router.put('/:cid', client.update);

    router.delete('/:cid', client.delete);

module.exports = router