const express = require('express')

const router = express.Router()

const Office = require('../../controller/office.controller');

    // router.get('/filter', Office.filter);

    router.get('/', Office.findAll);

    router.post('/', Office.create);

    router.get('/:oid', Office.findById);

    router.put('/:oid', Office.update);

    router.delete('/:oid', Office.delete);

    

module.exports = router