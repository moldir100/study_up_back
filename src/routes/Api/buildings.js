const express = require('express')

const router = express.Router()

const building = require('../../controller/building.controller');

    router.get('/', building.findAll);

    router.post('/', building.create);

    router.get('/:bid', building.findById);

    router.put('/:bid', building.update);

    router.delete('/:bid', building.delete);

module.exports = router