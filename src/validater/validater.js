const { body, validationResult } = require("express-validator");

exports.createRequestSchema = [
  body("DateCreate", "Please enter a valid DataCreate").exists(),
  body("DateStart", "DataStart is not valid").exists(),
  body("DateFinish", "DateFinish is not valid").exists(),
];

//   Promise.all(Promises.map(function(promise) {
//     return promise.reflect();
// })).each(function(inspection) {
//     if (inspection.isFulfilled()) {
//         console.log("Success...");
//     } else {
//         console.error("Reject...");
//     }
// });

exports.validate = (validators) => async (req, res, next) => {
  await Promise.all(validators.map((validator) => validator.run(req)));
  const errors = validationResult(req);
  if (errors.isEmpty()) return next();
  return res.json("No fields");
};
