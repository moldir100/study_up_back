"use strict";

const Courses = require("../model/courses.model");

exports.findAll = function (req, res) {
	Courses.findAll(function (err, courses) {
		if (err) res.send(err);
		console.log("courses", courses)
		res.send({
			status:true,
			data: courses
		});
	});
};

exports.findTeacherCourses = function (req, res) {
	Courses.findTeacherCourses(req.params.u_id, function (err, courses) {
		if (err) res.send(err);
		console.log("courses",courses)
		res.json({
			status: true,
			data: courses
		});
	});
};

exports.aboutCourse = function (req, res) {
	Courses.aboutCourse(req.params.course_id, function (err, courses) {
		if (err) res.send(err);
		console.log("courses",courses)
		res.json({
			status: true,
			data: courses
		});
	});
};

exports.create = function (req, res) {
	const new_Courses = new Courses(req.body);
	console.log("#test");
	if (
		typeof req.body.constructor === "object" &&
		Object.keys(req.body).length === 0
	) {
		return res
			.status(400)
			.send({ error: true, message: "Please provide all required field" });
	} else {
		Courses.create(new_Courses, function (err, courses) {
			if (err) res.send(err);
			res.json({
				error: false,
				message: "Base added successfully!",
				data: courses,
			});
		});
	}
};

exports.addCourseLesson = function (req, res) {
	const new_Courses = new Courses(req.body);
	console.log("#test");
	if (
		typeof req.body.constructor === "object" &&
		Object.keys(req.body).length === 0
	) {
		return res
			.status(400)
			.send({ error: true, message: "Please provide all required field" });
	} else {
		Courses.create(new_Courses, function (err, Courses) {
			if (err) res.send(err);
			res.json({
				error: false,
				message: "Lesson added successfully!",
				data: Courses,
			});
		});
	}
};

exports.findById = function (req, res) {
	Courses.findById(req.params.u_id, function (err, courses) {
		if (err) res.send(err);
		console.log("courses",courses)
		res.json({
			status: true,
			data: courses
		});
	});
};

exports.courseLessons = function (req, res) {
	Courses.courseLessons(req.params.course_id, function (err, lessons) {
		if (err) res.send(err);
		console.log("lessons",lessons)
		res.json({
			status: true,
			lessons: lessons
		});
	});
};

exports.getSupplyRemove = async (req, res, next) => {
	try {
		const {result, total} = await crtr.supplyRemove2(req.body, {station: req.userInfo.station});
		const html = await pdf.getHtmlFromTemplate(cwd+'/views/supplyRemove.ejs', {result, contractor, dateTo, dateFrom, total, moment})
		res.contentType('application/pdf').send(await pdf.getPdfBuffer(html, consts.textOptions));
	} catch (error) {
		return next(consts.serverErr);
	}
}

exports.update = function (req, res) {
	if (
		typeof req.body.constructor === "object" &&
		Object.keys(req.body).length === 0
	) {
		return res
			.status(400)
			.send({ error: true, message: "Please provide all required field" });
	} else {
		Courses.update(
			req.params.bid,
			new Courses(req.body),
			function (err, courses) {
				if (err) res.send(err);
				res.json({ error: false, message: "Base successfully updated" });
			}
		);
	}
};

exports.delete = function (req, res) {
	Courses.delete(req.params.course_id, function (err, courses) {
		if (err) res.send(err);
		res.json({ error: false, message: "Base successfully deleted" });
	});
};
