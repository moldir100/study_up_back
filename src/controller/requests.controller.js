"use strict";
const Request = require("../model/requests.model");

exports.findAll = function (_, res, next) {
  Request.findAll(function (err, request) {
    if (err) return next(new Error(err));
    res.send(request);
  });
};

// if (!errors.isEmpty()) {
//   return res.status(400).json({ errors: errors.array() });

exports.create = function (req, res, next) {
  const new_request = new Request(req.body);
  //handles null error
  Request.create(new_request, function (err, request) {
    if (err) return next(new Error(err));
    res.json({ status: true });
  });
};

exports.findById = function (req, res, next) {
  Request.findById(req.params.rid, function (err, request) {
    if (err) return next(new Error(err));
    res.json(request);
  });
};

exports.update = function (req, res, next) {
  if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
    res
      .status(400)
      .send({ error: true, message: "Please provide all required field" });
  } else {
    Request.update(
      req.params.rid,
      new Request(req.body),
      function (err, _) {
        if (err) return next(new Error(err));
        res.json({ error: false, message: "request successfully updated" });
      }
    );
  }
};

exports.delete = function (req, res, next) {
  Request.delete(req.params.rid, function (err, _) {
    if (err) return next(new Error(err));
    res.json({ error: false, message: "request successfully deleted" });
  });
};
