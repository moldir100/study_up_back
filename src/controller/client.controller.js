"use strict";

const Client = require("../model/client.model");
// const clientFilter = require('../model/client.filter.model');

exports.findAll = function (req, res) {
  Client.findAll(function (err, client) {
    if (err) res.send(err);
    res.send(client);
  });
};

exports.create = function (req, res) {
  const new_client = new Client(req.body);
  console.log("#test");
  if (
    typeof req.body.constructor === "object" &&
    Object.keys(req.body).length === 0
  ) {
    return res
      .status(400)
      .send({ error: true, message: "Please provide all required field" });
  } else {
    Client.create(new_client, function (err, client) {
      if (err) res.send(err);
      res.json({
        error: false,
        message: "Base added successfully!",
        data: client,
      });
    });
  }
};

exports.findById = function (req, res) {
  Client.findById(req.params.cid, function (err, client) {
    if (err) res.send(err);
    res.json(client);
  });
};
exports.update = function (req, res) {
  if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
    return res
      .status(400)
      .send({ error: true, message: "Please provide all required field" });
  } else {
    Client.update(req.params.cid, new Client(req.body), function (err, client) {
      if (err) res.send(err);
      res.json({ error: false, message: "Base successfully updated" });
    });
  }
};

exports.delete = function (req, res) {
  Client.delete(req.params.cid, function (err, client) {
    if (err) res.send(err);
    res.json({ error: false, message: "Base successfully deleted" });
  });
};

// exports.filter = function(req, res) {
//     officeFilter.filter(req.query,function(err, office) {
//         console.log('controller')

//        if (err)
//         res.send(err);
//           console.log('res', office);
//             res.send(office);
//       });
//     }

/*
    exports.filter = function(req, res) {
        clientFilter.filter(req.query,function(err, client) {
            console.log('controller')
            
           if (err)
            res.send(err);
              console.log('res', client);
                res.send(client);
          });
    };
*/

// exports.filter = function(req, res) {
//    clientFilter.filter(req.query,function(err,client) {
//        if (err)
//         res.send(err);
//         let count = 0;
//             if(client && client !== null){
//            client.forEach(element =>
//                 count = count + element.money
//             )
//         }

//         res.send({
//            client:client,
//             count: count
//         });
//        clientFilter.count(req.query,function(err, count) {
//             if (err)
//             res.send(err);

//             const result = Object.values(JSON.parse(JSON.stringify(count)));
//             console.log('#result',result)
//             console.log('#result',result[0])

//             let mapped = Object.values(result[0])
//             res.send({
//                client:client,
//                 count: mapped});
//         })
//     });
// };
// const where = {
//     from: {
//         $between: [startDate, endDate]
//     }
// };
