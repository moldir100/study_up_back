"use strict";

const User = require("../model/user.model");

exports.findAll = function (req, res) {
    User.findAll(function (err, user) {
        if (err) res.send(err);
        res.send(user);
    });
};

const redis = require('../utils/redis');
const af = require('../fn/auth');
const log = console.log;
const chalk = require('chalk');
const consts = require("../utils/constants");
const moment = require("moment");
const {request} = require("express");
const dbConn = require("../../config/config");
const jwt = require("jsonwebtoken");

const TOKEN_SECRET = '1a2b-3c4d-5e6f-7g8h'

exports.create = function (req, res) {
    const new_User = new User(req.body);
    if (
        typeof req.body.constructor === "object" &&
        Object.keys(req.body).length === 0
    ) {
        return res
            .status(400)
            .send({ error: true, message: "Please provide all required field" });
    } else {
        User.create(new_User, function (err, User) {
            if (err) res.send(err);

            let data = {
                u_id: '',
                name: '',
                surname: '',
                nickName: '',
                email: '',
                role_id: '',
                address: '',
                contacts: '',
            }
            data.name = req.body.name
            data.surname = req.body.surname
            data.nickName = req.body.nickName
            data.email = req.body.email
            data.role_id = req.body.role_id
            data.address = req.body.address
            data.about = req.body.about
            data.contacts = req.body.contacts

            function generateAccessToken(user) {
                return jwt.sign(user, TOKEN_SECRET, { expiresIn: '1800s' });
            }

            const token = generateAccessToken({ user: req.body.user });
            // res.json(token);

            console.log("req.body", req.body);
            console.log("token", token)
            console.log("user", data)
            res.json({
                status: true,
                error: false,
                message: "Base added successfully!",
                user: data,
                token: token
            });
        });
    }
};

// exports.findById = function (req, res) {
//     User.findById(req.params.bid, function (err, user) {
//         if (err) res.send(err);
//         res.json(user);
//     });
// };

exports.update = function (req, res) {
    if (
        typeof req.body.constructor === "object" &&
        Object.keys(req.body).length === 0
    ) {
        return res
            .status(400)
            .send({ error: true, message: "Please provide all required field" });
    } else {
        User.update(
            req.params.bid,
            new User(req.body),
            function (err, user) {
                if (err) res.send(err);
                res.json({ error: false, message: "Base successfully updated" });
            }
        );
    }
};

exports.delete = function (req, res) {
    User.delete(req.params.bid, function (err, user) {
        if (err) res.send(err);
        res.json({ error: false, message: "Base successfully deleted" });
    });
};

exports.postLogin = (req, res) => {
    af.auth(req.body.email, req.body.password)
        .then(u => {
            redis.set(`token_${u.token}`, JSON.stringify(u));
            // redis.set(`user_${u.user.id}`, JSON.stringify(u));
            redis.set(`token_contractors_${u.token}`, JSON.stringify(u.lists.user));

            if (u.status) {
                redis.set(`token_${u.token}`, JSON.stringify(u));
                redis.set(`user_${u.user.id}`, JSON.stringify(u));
                redis.set(`token_contractors_${u.token}`, JSON.stringify(u.lists.user));

                // if (+u.user.roleId === 3 || +u.user.roleId === 7) {
                //     redis.set(`token_contractors_${u.token}`, JSON.stringify(u.lists.contractors));
                // }
            }
            res.json(u);
        })
        .catch(e => {
            log(chalk.white.bgRed.bold('login error:', e));
            res.json({status: false, token: null, user: null});
        });
}

exports.getLogout = (req, res) => {
    log('logout, token:', req.header('X-Auth'));
    if (req.header('X-Auth')) {
        redis.del(`token_${req.header('X-Auth')}`);
        redis.del(`user_${req.header('X-Auth')}`);
        res.status(200).send({status: true, response: {msg: 'logout true'}});
    } else {
        res.status(200).send({status: false, error: {code: 'redis_error', msg: 'Ошибка при удалении токена'}});
    }
}

exports.putAcceptOffer = async (req, res, next) => {
    try {
        const userObj = JSON.parse(await redis.get(`token_${req.headers['x-auth']}`));
        userObj.user.offer = (await af.acceptOffer(userObj.user.id))[0] ? 1 : 0;
        await redis.del(`token_${req.headers['x-auth']}`);
        await redis.set(`token_${req.headers['x-auth']}`, JSON.stringify(userObj));
        res.status(200).send({status: true});
    } catch(e) {
        next(consts.serverErr)
    }
}

exports.verifyTsp = async (req, res, next) => {
    if (!req.body.hasOwnProperty('hash')) return consts.fieldErr;
    try {
        const result = await reg.verifyTsp(req.body.hash, req.body.agreement);
        if (!result) return next(consts.contractNotFound)
        res.json({status: true, response: result})
    } catch(e) {
        return next(consts.serverErr);
    }
}

exports.postSignup = async (req, res, next) => {
    try {
        const result = await af.signup(req.body);
        res.json({status: true, response: result})
    } catch(e) {
        return next(consts.serverErr)
    }
}

