const {body, param, checkSchema, validationResult} = require('express-validator');

const consts = require('../utils/constants');


exports.loginSchema = [
  body('email', 'Please enter a valid email.').not().isEmpty().trim(),
  body('password', 'password is not valid').not().isEmpty().trim(),
];

exports.genSettingsSchema = [
  body('companyName', 'Please enter a valid company name.').exists(),
  body('actualAddress', 'actual address is not valid').exists(),
  body('legalAddress', 'Please enter a valid Address.').exists(),
  body('phone', 'phone is not valid').exists(),
  body('email', 'Please enter a valid email.').exists(),
  param('id', 'id is not valid').exists(),
];

exports.requisitSchema = [
  body('bankId', 'Please enter a valid bankId').exists(),
  body('iik', 'account is not valid').exists(),
  param('id', 'id is not valid').exists(),
];

exports.contactSchema = [
  body('name', 'Please enter a valid name').exists(),
  body('phone', 'phone is not valid').exists(),
  body('email', 'email is not valid').exists(),
  param('id', 'id is not valid').exists(),
];

exports.changePassSchema = [
  body('password', 'Please enter a valid password').exists(),
  body('oldPassword', 'oldPassword is not valid').exists(),
  param('userId', 'id is not valid').exists(),
];

exports.createSupplySchema = [
  body('data', 'Please enter a valid data').exists(),
  param('userId', 'userId is not valid').exists(),
];

exports.changeStatusSchema = [
  body('wagonsIds', 'Please enter a valid wagonsIds').exists(),
  body('requestId', 'Please enter a valid requestId').exists(),
  param('userId', 'userId is not valid').exists(),
  param('statusId', 'statusId is not valid').exists(),
];

exports.acceptKjtDisp = [
  body('wagonsIds', 'Please enter a valid wagonsIds').exists(),
  body('type', 'Please enter a valid type').exists(),
  body('userId', 'userId is not valid').exists(),
];

exports.dispDoneSchema = [...this.acceptKjtDisp, body('requestId', 'requestId is not valid').exists()];

exports.dispSupplySchema = [...this.createSupplySchema, param('requestId', 'requestId is not valid').exists()];

exports.editManeuverSchema = [
  param('requestId', 'requestId is not valid').exists(),
  param('userId', 'userId is not valid').exists(),
];

exports.contractorSchema = [
  body('bin', 'bin is not valid').exists(),
  body('companyName', 'Please enter a valid companyName').exists(),
  body('deadEnds', 'deadEnds is not valid').exists(),
];

exports.editContractorSchema = [...this.contractorSchema, param('id', 'id is not valid')];

exports.patchChangeSchema = [
  body('type', 'Please enter a valid type').exists(),
  body('statusId', 'Please enter a valid statusId').exists(),
  body('info', 'info is not valid').exists(),
  body('wagons', 'Please enter a valid wagons').exists(),
  body('userId', 'userId is not valid').exists(),
];

exports.moveWagonSchema = checkSchema({
  to: {
    in: 'body',
    exists: {errorMessage: 'Field `to` cannot be empty'},
  },
  'to.lenPathId': {exists: {errorMessage: 'Field `lenPathId` cannot be empty'}},
  'to.deadEndId': {exists: {errorMessage: 'Field `lenPathId` cannot be empty'}},
  'to.moveDt': {exists: {errorMessage: 'Field `moveDt` cannot be empty'}},
  'to.info': {exists: {errorMessage: 'Field `info` cannot be empty'}},
  'to.wagonId': {exists: {errorMessage: 'Field `wagonId` cannot be empty'}},
  'to.index': {exists: {errorMessage: 'Field `index` cannot be empty'}},
  'to.typeManeuver': {exists: {errorMessage: 'Field `typeManeuver` cannot be empty'}},
});

exports.resetActiveSchema = [
  body('deadEndId', 'deadEndId is not valid').exists(),
  body('id', 'Please enter a valid id').exists(),
  body('type', 'type is not valid').exists(),
];

exports.changeView = [
  body('id', 'Please enter a valid request id').exists(),
];

// exports.resetActiveSchema = [
//   body('reqDeadEndId', 'reqDeadEndId is not valid').exists(),
//   body('listWagonId', 'Please enter a valid listWagonId').exists(),
//   body('type', 'type is not valid').exists(),
//   body('listDeadEndId', 'listDeadEndId is not valid').exists(),
//   body('contractorId', 'contractorId is not valid').exists(),
//   body('id', 'id is not valid').exists(),
// ];

exports.createContractorSchema = [
  body('bin', 'Please enter a valid bin.').exists(),
  body('email', 'email is not valid').exists(),
  body('companyName', 'companyName is not valid').exists(),
  body('actualAdress', 'Please enter a valid actualAddress.').exists(),
  body('legalAdress', 'legalAddress is not valid').exists(),
  body('phone', 'phone is not valid').exists(),
  body('signReason', 'Please enter a valid signReason.').exists(),
  body('firstHead', 'legalAddress is not valid').exists(),
  body('userCompanyId', 'userCompanyId is not valid').exists(),
];

const contractSchema = [
  body('deadEndId', 'Please enter a valid deadEndId.').exists(),
  body('tariffId', 'tariffId is not valid').exists(),
  body('contractText', 'contractText is not valid').exists(),
  body('dt', 'Please enter a valid dt.').exists(),
  body('dtEnd', 'Please enter a valid dtEnd.').exists(),
  body('requisiteId', 'requisiteId is not valid').exists(),
  body('contactId', 'contactId is not valid').exists(),
  body('discount', 'discount is not valid').exists(),
]

exports.createContractSchema = [param('id', 'id is not valid').exists(), ...contractSchema];

exports.editContractSchema = [
    ...contractSchema,
  body('id', 'Please enter a valid id.').exists(),
  body('contractorId', 'contractorId is not valid').exists(),
]

exports.cancelContractSchema = [
  body('contractId', 'Please enter a valid id.').exists(),
  body('contractorId', 'contractorId is not valid').exists(),
  body('comment', 'Please enter a valid id.').exists(),
]

exports.clientContractSchema = [
  body('contractId', 'Please enter a valid id.').exists(),
  body('clientSignedXml', 'contractorId is not valid').exists(),
  body('contractorId', 'Please enter a valid id.').exists(),
]

exports.managerContractSchema = [
  body('id', 'Please enter a valid id.').exists(),
  body('password', 'contractorId is not valid').exists(),
]

exports.createAgreementSchema = [
  body('dateStart', 'Please enter a valid deadEndId.').exists(),
  body('htmlText', 'tariffId is not valid').exists(),
  body('contractId', 'contractText is not valid').exists(),
  body('number', 'Please enter a valid dt.').exists(),
  body('contractorId', 'Please enter a valid dtEnd.').exists()
]

exports.cancelAgreementSchema = [
  body('id', 'Please enter a valid id.').exists(),
  body('contractorId', 'contractorId is not valid').exists(),
  body('comment', 'Please enter a valid id.').exists(),
]

exports.editAgreementSchema = [
    ...this.createAgreementSchema,body('id', 'Please enter id.').exists()
]

exports.clientAgreementSchema = [
  body('id', 'Please enter a valid id.').exists(),
  body('clientXml', 'Please enter a clientXml.').exists(),
]

exports.validate = (validators, onFail) => {
  return async (req, res, next) => {
    await Promise.all(validators.map(validator => validator.run(req)));
    const errors = validationResult(req);
    if (errors.isEmpty()) return next();
    return onFail ? res.json(onFail) : next(consts.fieldErr);
  };
};