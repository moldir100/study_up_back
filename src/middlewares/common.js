const api = require('../fn/api');
const af = require("../fn/auth.js");
const of = require("../fn/other.js");
const consts = require('../utils/constants');
const redis = require('../utils/redis');

const mode = '' + process.env.NODE_ENV === 'prod';

exports.logReport = async (req, _, next) => {
	const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	try {
		await of.logMiddle(req.method, req.url, req.body, req.header('X-Auth'), req.get('User-Agent'), ip);
		if (!req.header('X-Auth')) return next(consts.authErr)
		const r = await af.checkToken(req.header('X-Auth'));
		if (!r) return next(consts.authErr);
		req.userInfo = r;
		next();
	} catch (e) {
		return next(e)
	}
}

exports.disableCache = (req, res, next) => {
	res.setHeader('Surrogate-Control', 'no-store');
	res.setHeader('Cache-Control', 'no-store, no-cache, must-revalidate, proxy-revalidate');
	res.setHeader('Pragma', 'no-cache');
	res.setHeader('Expires', '0');
	next();
}

exports.checkOffer = (req, res, next) => {
	if (req.method !== 'PATCH' && req.method !== 'POST' && req.method !== 'PUT') {
		return next();
	}
	redis.get(`token_${req.headers['x-auth']}`, (err, result) => {
		if (err) return next(consts.authErr);
		const userObj = JSON.parse(result);
		if (result != null && userObj.user) {
			if (!userObj.user.offer && userObj.user.roleId === 1) return next(consts.offerErr);
			next();
		} else {
			return next(consts.authErr);
		}
	});
}

exports.onlyManagerCan = (req, _, next) => {
	if (+req.userInfo.roleId !== 7) return next(consts.managerErr)
	next();
}

exports.onlyClientCan = (req, _, next) => {
	if (+req.userInfo.roleId !== 1) return next(consts.managerErr)
	next();
}

exports.onlyDispatcherCan = (req, _, next) => {
	if (+req.userInfo.roleId !== 3) return next(consts.managerErr)
	next();
}

exports.logToTelegram = async (req, res, next) => {
	try {
		await api.logToTelegram(req.body.text, mode);
		res.json({status: true, response: {message: 'success'}});
	} catch(e) {
		next(consts.serverErr)
	}
}
