const jwt = require("jsonwebtoken");

const config = process.env;

const TOKEN_SECRET = '1a2b-3c4d-5e6f-7g8h'
const verifyToken = (req, res, next) => {
	const token =
		req.body.token || req.query.token || req.headers["x-access-token"];

	if (!token) {
		return res.status(403).send("A token is required for authentication");
	}
	try {
		const decoded = jwt.verify(token, TOKEN_SECRET);
		res.user = decoded.user;
	} catch (err) {
		return res.status(401).send(
			{
				status: false,
				data: "Invalid Token"
			});
	}
	return next();
};

module.exports = verifyToken;